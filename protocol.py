#!/usr/bin/python

import struct
import nbt
import zlib
import zlib
from sys import stderr, stdout, exit

# protocol version
# (the client will be kicked if LoginPacket doesn't send this one)
VERSION = 23

def handle_incomplete(fun):
    def new_fun(self, data):
        try:
            return fun(self, data)
        except struct.error:
            raise IncompletePacket()
    return new_fun


class IncompletePacket(Exception):
    pass


class Data(object):
    type_length = {
            # byte, unsigned byte
            "b": 1, "B": 1,
            # short
            "h": 2, "H": 2,
            # int
            "i": 4, "I": 4,
            # long long
            "q": 8, "Q": 8,
            # float, double
            "f": 4, "d": 8,
    }

    def __init__(self, data):
        self.d = data

    def __call__(self, length):
        d, self.d = self.d[:length], self.d[length:]
        return d

    def get_string(self):
        length = struct.unpack("!H", self(2))[0]
        return fmtuni(struct.unpack("!%ds" % (2 * length),
                    self(length * 2))[0])

    def get_value(self, type_):
        if type_ == "slot":
            return self.get_slot()
        elif type_ == "s":
            return self.get_string()
        elif type_ == "mdata":
            return self.get_metadata()
        return struct.unpack("!" + type_, self(self.type_length[type_]))[0]

    def get_metadata(self):
        """
        return the entity metadata contained in data
        """
        metadata = {}
        # the index of the data and it's type are packed into one unsigned byte
        x = self.get_value("B")
        T = ("b", "h", "i", "f")
        while x != 127:
            # if x is 127, there is no more data to get
            # else, we unpack it and get the associated
            index = x & 0x1f
            type_ = x >> 5
            if type_ < 4:
                val = self.get_value(T[type_])
            elif type_ == 4:
                val = self.get_string()
            elif type_ == 5:
                val = {}
                val["id"] = self.get_value("h")
                val["count"] = self.get_value("b")
                val["damage"] = self.get_value("h")
            elif type_ == 6:
                val = [self.get_value("i") for i in xrange(3)]
            metadata[index] = val
            x = self.get_value("B")
        return metadata

    def get_slot(self):
        """
        A slot data structure is an item and it's content
        """
        # get the item id
        items = []
        enchantable_items = [
                0x103, #Flint and steel
                0x105, #Bow
                0x15A, #Fishing rod
                0x167, #Shears

                #TOOLS
                #sword, shovel, pickaxe, axe, hoe
                0x10C, 0x10D, 0x10E, 0x10F, 0x122, #WOOD
                0x110, 0x111, 0x112, 0x113, 0x123, #STONE
                0x10B, 0x100, 0x101, 0x102, 0x124, #IRON
                0x114, 0x115, 0x116, 0x117, 0x125, #DIAMOND
                0x11B, 0x11C, 0x11D, 0x11E, 0x126, #GOLD

                #ARMOUR
                #helmet, chestplate, leggings, boots
                0x12A, 0x12B, 0x12C, 0x12D, #LEATHER
                0x12E, 0x12F, 0x130, 0x131, #CHAIN
                0x132, 0x133, 0x134, 0x135, #IRON
                0x136, 0x137, 0x138, 0x139, #DIAMOND
                0x13A, 0x13B, 0x13C, 0x13D  #GOLD
        ]
        x = self.get_value("h")
        while x != -1:
            item = {"id": x}
            item["count"] = self.get_value("b")
            item["damage"] = self.get_value("h")
            if x in enchantable_items:
                length = self.get_value("h")
                enchantments = []
                if length > 0:
                    data = self.get_array("b", length)
                    file_ = StringIO("".join(chr(i) for i in data))
                    nbtfile = nbt.NBTFile(fileobj=file_)
                    nbtfile.pretty_tree()

                enchantements[1]
                item["enchantements"] = enchantements

            items.append(item)
        return items

    def get_array(self, type_, length):
        return [self.get_value(type_) for i in xrange(length)]



class Chunk(object):
    """
    A chunk of the world
    """

    def __init__(self, raw_data, (x, y ,z), (sx, sy, sz)):
        size = (sx + 1) * (sy + 1) * (sz + 1)
        block_types = raw_data[:size]
        block_mdatas = raw_data[size:int(1.5*size)]
        block_lights = raw_data[int(1.5*size):2*size]
        sky_lighs = raw_data[2*size:]
        self.size = (sx, sy, sz)
        self.pos = (x, y, z)
        self.data = raw_data


class Packet(object):
    """
    Build a packet by using the init dict:
        p = FooPacket(name="foo", id=12)

    Parse a packet by passing a string argument
        p = FooPacket("\x00\x01...")
        print p.id, p.name
    """

    # the remaining data after parsing
    remain = ""
    def __init__(self, logger, data=None, **args):
        if data is None:
            for k, v in args.items():
                setattr(self, k, v)
        else:
            self.build(data)
        self.logger = logger

    def __str__(self):
        fmt = ["!b"]
        values = [self.id]
        for name, type_ in self.format:
            if type_ == "s":
                fmt.append("H")
                if not hasattr(self, name):
                    length = 0
                    value = ""
                else:
                    value = getattr(self, name)
                    length = len(value)
                values.append(length)
                fmt.append("%ds" % (2 * length))
                values.append(str(fmtstr(value)))
            else:
                fmt.append(type_)
                value = getattr(self, name) if hasattr(self, name) else 0
                values.append(value)
        return struct.pack("".join(fmt), *values)

    @handle_incomplete
    def build(self, raw_data):
        data = Data(raw_data)
        data.get_value("b")
        try:
            for name, type_ in self.format:
                # if it is a string, the data contains its length followed by
                # the string
                if type_ == "s":
                    value = data.get_string()
                elif type_ == "mdata":
                    value = data.get_metadata()
                elif type_ == "slot":
                    value = data.get_slot()
                else:
                    value = data.get_value(type_)
                stderr.write("set %s = %s\n" % (name, value))
                setattr(self, name, value)
        except struct.error:
            raise IncompletePacket
        finally:
            pass

        self.remain = data.d


class KeepalivePacket(Packet):
    id = 0x00
    format = ("kid", "i"),


class LoginPacket(Packet):
    id = 0x01
    format = (
                # in s -> c packets, this is the player entity id.
                ("version", "i"),
                ("username", "s"),
                ("map_seed", "q"),
                # DEFAULT or SUPERFLAT (1.0.1)
                ("level_type", "s"),
                # 0 for survival, 1 for creative
                ("server_mode", "i"),
                # -1: The Nether, 0: The Overworld, 1: The End
                ("dimension", "b"),
                # 0 thru 3 for Peaceful, Easy, Normal, Hard
                ("difficulty", "b"),
                ("world_height", "B"),
                ("max_players", "B"),
    )


class HandshakePacket(Packet):
    id = 0x02
    format = ("name", "s"),


class ChatMessagePacket(Packet):
    id = 0x03
    format = ("message", "s"),


class TimeUpdatePacket(Packet):
    id = 0x04
    format = ("time", "q"),


class EntityEquipmentPacket(Packet):
    id = 0x05
    format = (
            ("eid", "i"),
            ("slot", "h"),
            ("item_id", "h"),
            ("damage", "h")
    )

class SpawnPositionPacket(Packet):
    id = 0x06
    format = (
            ("x", "i"),
            ("y", "i"),
            ("z", "i"),
    )


class UseEntityPacket(Packet):
    id = 0x07
    format = (
            ("user", "i"),
            ("target", "i"),
            ("left_click", "b"),
    )


class UpdateHealthPacket(Packet):
    id = 0x08
    format = (
            ("health", "h"),
            ("food", "h"),
            ("food_saturation", "f"),
    )


class RespawnPacket(Packet):
    id = 0x09
    format = (
            # -1: The Nether, 0: The Overworld, 1: The End
            ("dimension", "b"),
            # 0 thru 3 for Peaceful, Easy, Normal, Hard
            ("difficulty", "b"),
            # 0 for survival, 1 for creative
            ("server_mode", "b"),
            ("world_height", "h"),
            ("map_seed", "q"),
            ("level_type", "s"),
    )


class PlayerPacket(Packet):
    id = 0x0a
    format = ("on_ground", "b"),


class PlayerPositionPacket(Packet):
    id = 0x0b
    format = (
            ("x", "d"),
            ("y", "d"),
            ("stance", "d"),
            ("z", "d"),
            ("on_ground", "b"),
    )


class PlayerLookPacket(Packet):
    id = 0x0c
    format = (
            ("yaw", "f"),
            ("pitch", "f"),
            ("on_ground", "b"),
    )


class PlayerPosLookPacket(Packet):
    id = 0x0d
    format = (
            ("x", "d"),
            # WARNING: the fields are not in the same order when the client
            # sends it! y and stance are reversed
            ("stance", "d"),
            ("y", "d"),
            ("z", "d"),
            ("yaw", "f"),
            ("pitch", "f"),
            ("on_ground", "b"),
    )


class PlayerDiggingPacket(Packet):
    id = 0x0e
    format = (
            ("status", "b"),
            ("x", "i"),
            ("y", "b"),
            ("z", "i"),
            ("face", "b"),
    )


class PlayerBlockPlacementPacket(Packet):
    id = 0x0f
    format = (
            ("x", "i"),
            ("y", "b"),
            ("z", "i"),
            ("direction", "b"),
            ("held_item", "b"),
    )


class HoldingChangePacket(Packet):
    id = 0x10
    format = ("slot_id", "i"),


class UseBedPacket(Packet):
    id = 0x11
    format = (
            ("eid", "i"),
            ("in_bed", "b"),
            ("x", "i"),
            ("y", "b"),
            ("z", "i"),
    )


class AnimationPacket(Packet):
    id = 0x12
    format = (
            ("eid", "i"),
            ("animation", "b"),
    )


class EntityActionPacket(Packet):
    id = 0x13
    format = (
            ("eid", "i"),
            ("action_id", "b"),
    )


class NamedEntitySpawnPacket(Packet):
    id = 0x14
    format = (
            ("eid", "i"),
            ("player_name", "s"),
            ("x", "i"),
            ("y", "i"),
            ("z", "i"),
            ("rotation", "b"),
            ("pitch", "b"),
            ("curent_item", "h"),
    )


class PickupSpawnPacket(Packet):
    id = 0x15
    format = (
            ("eid", "i"),
            ("item", "h"),
            ("count", "b"),
            ("damage", "h"),
            ("x", "i"),
            ("y", "i"),
            ("z", "i"),
            ("rotation", "b"),
            ("pitch", "b"),
            ("roll", "b"),
    )


class CollectItemPacket(Packet):
    id = 0x16
    format = (
            ("collected_eid", "i"),
            ("collector_eid", "i"),
    )


class AddObjectPacket(Packet):
    id = 0x17
    format = (
            ("eid", "i"),
            ("type", "b"),
            ("x", "i"),
            ("y", "i"),
            ("z", "i"),
            # fireball thrower identity
            ("ft_eid", "i"),
            # these are set only if ft_eid > 0
            ("speed_x", "h"),
            ("speed_y", "h"),
            ("speed_z", "h"),
    )

    @handle_incomplete
    def build(self, raw_data):
        data = Data(raw_data)
        data.get_value("b")
        self.eid = data.get_value("i")
        self.type = data.get_value("b")
        self.x = data.get_value("i")
        self.y = data.get_value("i")
        self.z = data.get_value("i")
        self.ft_eid = data.get_value("i")
        if self.ft_eid > 0:
            self.speed_x = data.get_value("h")
            self.speed_y = data.get_value("h")
            self.speed_z = data.get_value("h")
        self.remain = data.d


class MobSpawnPacket(Packet):
    id = 0x18
    format = (
            ("eid", "i"),
            ("type", "b"),
            ("x", "i"),
            ("y", "i"),
            ("z", "i"),
            ("yaw", "b"),
            ("pitch", "b"),
            ("metadata", "mdata"),
    )


class PaintingPacket(Packet):
    id = 0x19
    format = (
            ("eid", "i"),
            ("title", "s"),
            ("x", "i"),
            ("y", "i"),
            ("z", "i"),
            ("direction", "i"),
    )


class ExperienceOrbPacket(Packet):
    id = 0x1a
    format = (
            ("eid", "i"),
            ("x", "i"),
            ("y", "i"),
            ("z", "i"),
            ("count", "h"),
    )


class EntityVelocityPacket(Packet):
    id = 0x1c
    format = (
            ("eid", "i"),
            ("velocity_x", "h"),
            ("velocity_y", "h"),
            ("velocity_z", "h"),
    )


class DestroyEntityPacket(Packet):
    id = 0x1d
    format = (
            ("eid", "i"),
    )


class EntityPacket(Packet):
    id = 0x1e
    format = (
            ("eid", "i"),
    )


class EntityRelativeMovePacket(Packet):
    id = 0x1f
    format = (
            ("eid", "i"),
            ("dx", "b"),
            ("dy", "b"),
            ("dz", "b"),
    )


class EntityLookPacket(Packet):
    id = 0x20
    format = (
            ("eid", "i"),
            ("yaw", "b"),
            ("pitch", "b"),
    )


class EntityLookRMovePacket(Packet):
    id = 0x21
    format = (
            ("eid", "i"),
            ("dx", "b"),
            ("dy", "b"),
            ("dz", "b"),
            ("yaw", "b"),
            ("pitch", "b"),
    )


class EntityTeleportPacket(Packet):
    id = 0x22
    format = (
            ("eid", "i"),
            ("x", "i"),
            ("y", "i"),
            ("z", "i"),
            ("yaw", "b"),
            ("pitch", "b"),
    )


class EntityStatusPacket(Packet):
    id = 0x26
    format = (
            ("eid", "i"),
            ("status", "b"),
    )


class AttachEntityPacket(Packet):
    id = 0x27
    format = (
            ("eid", "i"),
            ("vehicle_id", "i"),
    )


class EntityMetadataPacket(Packet):
    id = 0x28
    format = (
            ("eid", "i"),
            ("metadata", "mdata"),
    )


class EntityEffectPacket(Packet):
    id = 0x29
    format = (
            ("eid", "i"),
            ("effect_id", "b"),
            ("amplifier", "b"),
            ("duration", "h"),
    )


class RemoveEntityEffectPacket(Packet):
    id = 0x2a
    format = (
            ("eid", "i"),
            ("effect_id", "b"),
    )


class ExperiencePacket(Packet):
    id = 0x2b
    format = (
            ("xp_bar", "f"),
            ("level", "h"),
            ("total_xp", "h"),
    )


class PreChunkPacket(Packet):
    id = 0x32
    format = (
            ("x", "i"),
            ("z", "i"),
            # 0 = unload, 1 = initialize
            ("mode", "b"),
    )


class ChunkPacket(Packet):
    id = 0x33
    format = (
            ("x", "i"),
            ("y", "s"),
            ("z", "i"),
            ("size_x", "b"),
            ("size_y", "b"),
            ("size_z", "b"),
            ("size", "i"),
            # the data will be stored in the packet uncompressed
            ("chunk", "ba"),
    )

    @handle_incomplete
    def build(self, raw_data):
        data = Data(raw_data)
        data.get_value("b")
        dbg = "Got chunk: "
        self.x = x = data.get_value("i")
        self.y = y = data.get_value("h")
        self.z = z = data.get_value("i")
        dbg += "(%d, %d, %d), " % (x, y, z)
        self.size_x = sx = data.get_value("b")
        self.size_y = sy = data.get_value("b")
        self.size_z = sz = data.get_value("b")
        dbg += "(%d, %d, %d)" % (sx, sy, sz)
        l = self.size = data.get_value("i")
        content = zlib.decompress("".join(chr(n) for n in
            data.get_array("B", l)))
        self.chunk = Chunk(content, (x, y, z), (sx,sy, sz))
        dbg += " (size=%d)" % len(content)
        print dbg
        self.remain = data.d


class MultiBlockChangePacket(Packet):
    id = 0x34
    format = (
            ("chunk_x", ""),
            ("chunk_z", ""),
            ("array_length", "h"),
            ("coordinate_array", "sa"),
            ("type_array", "ba"),
            ("metadata_array", "ba"),
    )

    @handle_incomplete
    def build(self, raw_data):
        data = Data(raw_data)
        data.get_value("b")
        self.chunk_x = data.get_value("i")
        self.chunk_z = data.get_value("i")
        l = self.array_length = data.get_value("h")
        self.coordinate_array = data.get_array("h", l)
        self.type_array = data.get_array("b", l)
        self.metadata_array = data.get_array("b", l)
        self.remain = data.d


class BlockChangePacket(Packet):
    id = 0x35
    format = (
            ("x", "i"),
            ("y", "b"),
            ("z", "i"),
            ("type", "b"),
            ("metadata", "b"),
    )


class BlockActionPacket(Packet):
    id = 0x36
    format = (
            ("x", "i"),
            ("y", "h"),
            ("z", "i"),
            ("b1", "b"),
            ("b2", "b"),
    )


class ExplosionPacket(Packet):
    id = 0x3c
    format = (
            ("x", "d"),
            ("y", "d"),
            ("z", "d"),
            ("unknown", "f"),
            ("record_count", "i"),
            # a list of triplets
            ("records", "?"),
    )

    @handle_incomplete
    def build(self, raw_data):
        data = Data(raw_data)
        data.get_value("b")
        self.x = data.get_value("d")
        self.y = data.get_value("d")
        self.z = data.get_value("d")
        self.unknown = data.get_value("f")
        l = self.record_count = data.get_value("i")
        self.records = [data.get_array("b", 3) for i in xrange(l)]
        self.remain = data.d


class SoundEffectPacket(Packet):
    id = 0x3d
    format = (
            ("effect_id", "i"),
            ("x", "i"),
            ("y", "b"),
            ("z", "i"),
            ("data", "i"),
    )


class NewInvalidState(Packet):
    id = 0x46
    format = (
            ("reason", "b"),
            ("game_mode", "b"),
    )


class ThunderboltPacket(Packet):
    id = 0x47
    format = (
            ("eid", "i"),
            ("unknown", "b"),
            ("x", "i"),
            ("y", "i"),
            ("z", "i"),
    )


class OpenWindowPacket(Packet):
    id = 0x64
    format = (
            ("window_id", "b"),
            ("type", "b"),
            ("title", "s"),
            ("slots", "b"),
    )


class CloseWindowPacket(Packet):
    id = 0x65
    format = (
            ("window_id", "b"),
    )


class WindowClickPacket(Packet):
    id = 0x66
    format = (
            ("window_id", "b"),
            ("slot", "h"),
            ("right_click", "b"),
            ("action", "h"),
            ("shift", "b"),
            ("clicked_item", "slot"),
    )


class SetSlotPacket(Packet):
    id = 0x67
    format = (
            ("window_id", "b"),
            ("slot", "h"),
            ("slot_data", "slot"),
    )


class WindowItemPacket(Packet):
    id = 0x68
    format = (
            ("window_id", "b"),
            ("count", "h"),
            # an array of slots
            ("slot_data", "??"),
    )

    @handle_incomplete
    def build(self, raw_data):
        data = Data(raw_data)
        data.get_value("b")
        self.window_id = data.get_value("b")
        self.count = data.get_value("h")
        self.slot_data = data.get_array("slot", self.count)
        self.remain = data.d

class UpdateWindowPacket(Packet):
    id = 0x69
    format = (
            ("window_id", "b"),
            ("property", "h"),
            ("value", "h"),
    )


class TransactionPacket(Packet):
    id = 0x6a
    format = (
            ("window_id", "b"),
            ("action", "h"),
            ("accepted", "b"),
    )


class CreativeInventoryActionPacket(Packet):
    id = 0x6b
    format = (
            ("slot", "h"),
            ("item", "slot"),
    )


class EnchantItemPacket(Packet):
    id = 0x6c
    format = (
            ("window_id", "b"),
            ("enchantement", "b"),
    )


class UpdateSignPacket(Packet):
    id = 0x82
    format = (
            ("x", "i"),
            ("y", "h"),
            ("z", "i"),
            ("text1", "s"),
            ("text2", "s"),
            ("text3", "s"),
            ("text4", "s"),
    )


class ItemDataPacket(Packet):
    id = 0x83
    format = (
            ("type", "h"),
            ("item_id", "h"),
            ("text_length", "H"),
            ("text", "ba"),
    )

    @handle_incomplete
    def build(self, raw_data):
        data = Data(raw_data)
        data.get_value("b")
        self.type = data.get_value("h")
        self.item_id = data.get_value("h")
        l = self.text_length = data.get_value("B")
        self.text = data.get_array("b", l)
        self.remain = data.d

class IncrementStatisticsPacket(Packet):
    id = 0xc8
    format = (
            ("stats_id", "i"),
            ("amount", "b"),
    )


class PlayerListItemPacket(Packet):
    id = 0xc9
    format = (
            ("player_name", "s"),
            ("online", "b"),
            ("ping", "h"),
    )


class PluginMessagePacket(Packet):
    id = 0xfa
    format = (
            ("channel", "s"),
            ("length", "h"),
            ("data", "ba"),
    )

    @handle_incomplete
    def build(self, raw_data):
        data = Data(raw_data)
        data.get_value("b")
        self.channel = data.get_value("s")
        self.length = l = data.get_value("h")
        self.data = data.get_array("b", l)
        self.remain = data.d


class ServerListPingPacket(Packet):
    id = 0xfe
    format = ()


class KickPacket(Packet):
    id = 0xff
    format = ("reason", "s"),


def fmtstr(s):
    # change one-byte strings into short*
    return "".join("\x00%c" % c for c in s)

def fmtuni(u):
    return u[1::2]
