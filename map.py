#!/usr/bin/python

from datetime import datetime

class Map(object):
    chunks = {}
    size = 0
    def __init__(self):
        self.t = datetime.now()

    def store_chunk(self, chunk):
        pos = chunk.pos
        if pos in self.chunks:
            print "Chunk already here %s" % repr(pos)
        else:
            self.chunks[pos] = chunk
            self.size += len(chunk.data)
            t = datetime.now() - self.t
            delta = "%02d:%02d" % (t.seconds / 60, t.seconds % 60)
            print "Size of the map : %.2fMo, %s" % (self.size / (1024.0 ** 2), delta)

