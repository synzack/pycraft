#!/usr/bin/python

import socket
import binascii
from threading import Thread, Event
from time import sleep, strftime
from sys import stderr

import protocol
from protocol import *
from map import Map

HOOKS = {}

class Message(object):
    msgs = []
    def __init__(self):
        Event.__init__(self)

    def set(self, msg):
        self.msgs.append(msg)

    def get(self):
        try:
            return self.msgs.pop(0)
        except IndexError:
            return None

    def is_set(self):
        return self.msgs != []


class Logger(object):
    def __init__(self, log_level=0):
        self.level = log_level

    def info(self, msg):
        if self.level >= 0:
            print "%s [INFO] %s" % (strftime("%y-%m-%d %H:%M:%S"), msg)

    def debug(self, msg, level=3):
        if self.level >= level:
            stderr.write("%s [DEBUG] %s\n" % (strftime("%y-%m-%d %H:%M:%S"), msg))



class Sender(Thread):
    def __init__(self, bot, halt):
        self.bot = bot
        self.halt = halt
        Thread.__init__(self)

    def run(self):
        bot = self.bot
        m = bot.send_queue
        while not self.halt.is_set():
            if m.is_set():
                msg = bot.s.send(m.get())
            sleep(0.1)


class Reciever(Thread):
    def __init__(self, bot, halt):
        self.bot = bot
        self.halt = halt
        Thread.__init__(self)

    def run(self):
        bot = self.bot
        while not self.halt.is_set():
            bot._recv()
            sleep(0.5)

def _register_hook(packet_type):
    def register(fun):
        def new_fun(self, data):
            # The handle_packet method calls a hook with the data as
            # argument, and this decorator parses the packet
            # and retrieves the remaining data
            self.l.debug("Packet %s, %s called" % (packet_type.__name__,
                fun.__name__))
            packet = packet_type(self.l, data=data)
            fun(self, packet)
            return packet.remain
        if packet_type.id not in HOOKS:
            #print "registering %s -> %s" % (packet_type.__name__, fun.__name__)
            HOOKS[packet_type.id] = new_fun
        return new_fun
    return register


class Item(object):
    pass


class Entity(object):
    # a mob, or a player
    # x, y, z
    position = (0, 0, 0)
    # stance, yaw, pitch
    look = (0, 0, 0)
    on_ground = True
    def __init__(self, eid=0, position=(0, 0, 0), look=(0, 0, 0),
            on_ground=True):
        self.eid = eid
        self.position = position
        self.look = look
        self.on_ground = on_ground


class Client(Entity):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    stop = Event()
    map = Map ()
    send_queue = Message()
    connected = False
    halt = False

    def __init__(self, name, server, port=25565, logger=Logger ()):
        self.name = name
        self.server = server
        self.port = port

        Entity.__init__(self)

        self._register_default_hooks()

        self.connect()

        self.t_recv = Reciever(self, self.stop)
        self.t_recv.daemon = True
        self.t_recv.start()
        self.t_send = Sender(self, self.stop)
        self.t_send.daemon = True
        self.t_send.start()

        self.l = logger
        self.handshake()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def _recv(self, length=1024, handle=True):
        """
        If handle is false, the packet handler will not be called
        This is useful to ask for more data if a packet is incomplete
        """
        data = self.s.recv(length)
        if data:
            self.l.debug("Raw data recieved: %s (%d)" % (binformat(data),
                len(data)), 5)
        if handle:
            self._handle_packets(data)
        return data

    def _handle_packets(self, data):
        """
        Each chunk recieved has to be cut in packets
        The hooks have to return the remaining data
        """
        while data:
            try:
                if ord(data[0]) in HOOKS:
                    data = HOOKS[ord(data[0])](self, data)
                    self.l.debug("Remaining data: %s" % binformat(data), 4)
                else:
                    self.l.debug("No hook found for id %s" % binformat(data[0]), 1)
                    data = ""
            except IncompletePacket:
                self.l.debug("Need moar data.", 4)
                data += self._recv(handle=False)


    def _register_default_hooks(self):
        def default_hook(self, packet):
            pass

        for name in protocol.__dict__:
            cls = getattr(protocol, name)
            if hasattr(cls, "__base__") and cls.__base__ is protocol.Packet:
                _register_hook(cls)(default_hook)

    @_register_hook(KeepalivePacket)
    def _hook_keepalive(self, packet):
        self.send(KeepalivePacket(self.l, kid=packet.kid))

    @_register_hook(LoginPacket)
    def _hook_login(self, packet):
        self.l.debug("Logged in.", 1)
        self.connected = True

    @_register_hook(HandshakePacket)
    def _hook_handshake(self, packet):
        name = self.name[:16]
        # send client version
        self.send(LoginPacket(self.l, version=VERSION, username=self.name[:16]))

    @_register_hook(ChatMessagePacket)
    def _hook_chat_message(self, packet):
        color = False
        text = packet.message
        out_text = []
        for c in text:
            if c == "\xa7":
                color = True
            elif color:
                color = False
            else:
                out_text.append(c)
        self.l.info("Chat message: %s" % "".join(out_text))

    @_register_hook(TimeUpdatePacket)
    def _hook_time_update(self, packet):
        self.time = packet.time

    @_register_hook(PlayerPosLookPacket)
    def _hook_player_position_look(self, packet):
        self.position = packet.x, packet.y, packet.z
        self.look = packet.stance, packet.yaw, packet.pitch
        self.on_ground = packet.on_ground

        # those fields are reversed in the c -> s packet.
        packet.stance, packet.y = packet.y, packet.stance
        self.send(packet)

    @_register_hook(ChunkPacket)
    def _handle_chunk(self, packet):
        chunk = packet.chunk
        self.map.store_chunk(chunk)

    @_register_hook(UpdateHealthPacket)
    def _handle_health(self, packet):
        if packet.health <= 0:
            print "Dead"
            self.send(RespawnPacket(self.l))

    @_register_hook(RespawnPacket)
    def _handle_respawn(self, packet):
        print "Respawned"

    @_register_hook(KickPacket)
    def _handle_kick(self, packet):
        print "Kicked! (%s)" % packet.reason
        self.close()

    def send(self, packet):
        """
        Packets are wrote in a queue and sent by the thread
        """
        self.l.debug("Packet sent: %s" % binformat(str(packet)))
        self.send_queue.set(str(packet))
        return packet

    def update_player(self, position=None, look=None, on_ground=None):
        if on_ground is None:
            on_ground = self.on_ground
        else:
            self.on_ground = on_ground
        if position is not None and look is not None:
            self.position = x, y, z = position
            self.look = stance, yaw, pitch = look
            y, stance = stance, y
            packet = PlayerPosLookPacket(self.l, x=x, y=y, z=z, stance=stance,
                    yaw=yaw, pitch=pitch, on_ground=on_ground)
        elif position is not None:
            self.position = x, y, z = self.position
            stance = self.look[0]
            packet = PlayerPositionPacket(self.l, x=x, y=y, z=z, stance=stance,
                    on_ground=on_ground)
        elif look is not None:
            self.look[1:] = yaw, pitch = look[1:]
            packet = PlayerLookPacket(self.l, yaw=yaw, pitch=pitch,
                    on_ground=on_ground)
        else:
            packet = PlayerPacket(self.l, on_ground=on_ground)
        if self.connect:
            self.send(packet)

    def connect(self):
        self.s.connect((self.server, self.port))

    def handshake(self):
        self.send(HandshakePacket(self.l, name=self.name[:16]))

    def close(self):
        self.l.debug("Disconnecting", 1)
        self.t_send.halt.set()
        self.t_send.halt.set()
        self.s.close()
        self.connected = False
        self.halt = True

def binformat(data):
    return " ".join(("%02x" % ord(c)) for c in data)

if __name__ == "__main__":
    logger = Logger(4)
    with Client(u"poil", "demic.eu", logger=logger) as bot:
        sleep(1)
        while not bot.halt:
            bot.update_player(position=bot.position, look=bot.look)
            sleep(.5)

